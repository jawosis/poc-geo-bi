$( document ).ready(function() {

    $('iframe').each(function () {
        $(this).parent().prepend('<span class="togglebutton">Toggle</span>');
    });

    $("span.togglebutton").click(function () {
        $(this).parents("div.iframeWrapper").toggleClass("maximized");
    });




    function docLog(childNumber, outputfieldNumber, message, params) {
        var tag = 'output' + outputfieldNumber.toString() + 'child' + childNumber.toString();
        var output = "";
        console.log(tag);
        switch(message) {
          case "registered":
            output = "> Hi child" + childNumber.toString() + "! You are now registered."
            output += "\nParams sent from child: " + JSON.stringify(params);
        }
      
        var e = document.createElement('div');
        if (typeof e.innerText !== 'undefined') e.innerText = output;
        else e.textContent = output;
        document.getElementById(tag).appendChild(e);
    }

    function register(trans, args) {
      if (typeof args !== 'object') {
            throw [ "invalid_arguments:", 'arguments must be passed as Object!' ];
        }
      return docLog(args.childNumber, args.outputNumber, "registered", args.params)
    }
    var chan1 = Channel.build({
        debugOutput: true,
        window: document.getElementById("child1").contentWindow,
        origin: "*",
        scope: "testScope"
    });

    var chan2 = Channel.build({
        debugOutput: true,
        window: document.getElementById("child2").contentWindow,
        origin: "*",
        scope: "testScope"
    });
    chan1.bind("register", register);
    chan2.bind("register", register);

});

